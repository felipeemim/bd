-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: 26-Set-2018 às 12:50
-- Versão do servidor: 5.7.23-0ubuntu0.18.04.1-log
-- PHP Version: 5.6.37-1+ubuntu18.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `curso`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `curso_pessoa`
--

CREATE TABLE `curso_pessoa` (
  `id` int(11) NOT NULL,
  `nome` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `celular` varchar(255) NOT NULL,
  `dataNascimento` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `curso_pessoa`
--

INSERT INTO `curso_pessoa` (`id`, `nome`, `email`, `celular`, `dataNascimento`) VALUES
(1, 'Eron', 'fillipeeron@gmail.com', '9163873467', '2018-09-02'),
(2, 'maria sophia', 'mariasophia@gmail.com', '34565223', '2018-09-08'),
(3, 'MATEUS CARVALHO', 'mateuscarvalho@yahoo.com.br', '763888224', '2018-09-09'),
(4, 'jhessica matos', 'jhessica@hotmail.com', '446633434', '2018-07-08'),
(5, 'elizabeth monteiro', 'elizabeth@outlook.com', '39857892', '2018-09-24'),
(6, 'Elgenio Correa', 'elgenio@gmail.com', '3675434', '2018-04-09'),
(7, 'Diego da Silva', 'dsilva@hotmail.com', '(87)98369845', '2018-09-16'),
(8, 'Soraya Solânge', 'ssolange@gmail.com', '(11)872309099', '2018-02-20'),
(9, 'Bruno Anderson', 'bruninho@gmail.com', '(98)18982343', '2018-09-21'),
(10, 'Armando Almeida', 'allanufpa@ufpa.br', '(76)99124367', '2017-07-15'),
(11, 'Evandro Nunes', 'evannunes@hotmail.com', '(67)98766789', '2018-09-24'),
(12, 'Fabian Alberto', 'fabianfabian@outlook.com', '(12)22332456', '2018-04-18'),
(13, 'Mateus Maia', 'mmaia@hotmail.com', '(87)92343867', '2018-09-29'),
(14, 'Flavia Ribeiro', 'fribeiro@gmail.com', '(67)09092343', '2018-05-17'),
(15, 'Juan Barata', 'juanb@gmail.com', '(78)90234534', '2018-09-24'),
(16, 'Gilmar Tavares', 'giltava@yahoo.com.br', '(78)90465897', '2018-04-18'),
(17, 'Herinque Julian', 'heheju@hotmail.com', '(56)81207634', '2018-09-12'),
(18, 'Gerson Tavares', 'gersont@gmail.com', '(91)9187645', '2018-05-16'),
(19, 'Arthur Campus', 'arthurcampus@hotmail.com', '(91)8764928', '2018-09-28'),
(20, 'Mauro Magalhâes', 'mauro_maga@hotmail.com', '(91)45367278', '2018-06-13');

-- --------------------------------------------------------

--
-- Estrutura da tabela `curso_usuario`
--

CREATE TABLE `curso_usuario` (
  `id` int(11) NOT NULL,
  `login` varchar(255) NOT NULL,
  `senha` varchar(255) NOT NULL,
  `idPessoa` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `curso_usuario`
--

INSERT INTO `curso_usuario` (`id`, `login`, `senha`, `idPessoa`) VALUES
(1, 'filipeeron', '1234', 1),
(2, 'maria', 'maria9876', 2),
(3, 'carvalhosalinas', 'car3454', 3),
(4, 'jhessica3434', '34532', 4),
(5, 'elieli', '4453eli', 5),
(6, 'elgenio$gmail.com', 'elgenio', 6),
(7, 'diegoteste', 'ddddd123', 7),
(8, 'soraya', 'soso', 8),
(9, 'bruno123', '9827', 9),
(10, 'allan', 'allan23', 10),
(25, 'lorivaldo', 'lori2332', 25),
(26, 'mendes2342', 'mendes', 26),
(27, 'glenda', 'glenda33', 27),
(28, 'yumi', 'yumi3444', 28);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `curso_pessoa`
--
ALTER TABLE `curso_pessoa`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `curso_usuario`
--
ALTER TABLE `curso_usuario`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `curso_pessoa`
--
ALTER TABLE `curso_pessoa`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `curso_usuario`
--
ALTER TABLE `curso_usuario`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
